import {createStore} from 'vuex';

import pl from '../locales/pl.json';
import en from '../locales/en.json';
import de from '../locales/de.json';
import gb from '../locales/gb.json';

const store = createStore({
    state: {
        mobileMenu: {
            active: false
        },
        languages: [
            {
                slug: 'pl',
                label: 'PL',
                active: true,
                messages: pl
            },
            {
                slug: 'en',
                label: 'EN',
                active: false,
                messages: en
            },
            {
                slug: 'de',
                label: 'DE',
                active: false,
                messages: de
            },
            {
                slug: 'gb',
                label: 'GB',
                active: false,
                messages: gb
            },
        ], activeLang: {
            slug: 'pl',
            label: 'PL',
            active: true,
            messages: pl
        },

    },
    mutations: {
        set_mobileModalStatus: (state) => {
            state.mobileMenu.active = !state.mobileMenu.active;
        },
        set_activeLanguage: (state, data) => {
            state.languages.forEach(el => {
                if (el.slug === data.slug) {
                    state.activeLang = el;
                    el.active = true;
                } else
                    el.active = false;
            });
        }
    },
    getters: {},

    actions: {},
});

export default store;
